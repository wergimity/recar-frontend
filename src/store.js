import Vue from 'vue'
import Vuex from 'vuex'
import { requestStatus } from './util/request-status'
import { api } from './api'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    list: requestStatus(() => api.get(`parts`)),
    show: requestStatus(({ id }) => api.get(`parts/${id}`)),
    create: requestStatus(({ data }) => api.post(`parts`, { ...data })),
    update: requestStatus(({ id, data }) => api.patch(`parts/${id}`, { ...data })),
    remove: requestStatus(({ id }) => api.delete(`parts/${id}`)),
  },
})
