import axios from 'axios'

export const api = axios.create({
  baseURL: 'http://localhost:5000/api',
  headers: { Authorization: process.env.VUE_APP_SECRET },
})

window.api = api
