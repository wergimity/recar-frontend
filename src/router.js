import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/parts',
    },
    {
      path: '/parts',
      name: 'list',
      component: () => import('./views/index.vue'),
    },
    {
      path: '/parts/create',
      name: 'create',
      component: () => import('./views/create.vue'),
    },
    {
      path: '/parts/:id/delete',
      name: 'delete',
      component: () => import('./views/remove.vue'),
    },
    {
      path: '/parts/:id',
      name: 'show',
      component: () => import('./views/update.vue'),
    },
  ],
})
