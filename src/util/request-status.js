export const requestStatus = (request) => {
  return {
    namespaced: true,
    getters: {
      loading: (state) => state.loading,
      data: (state) => state.data,
      error: (state) => state.error,
    },
    actions: {
      async request({ commit }, payload) {
        try {
          commit('LOADING')
          const { data } = await request(payload)
          if (data.success) {
            commit('SUCCESS', data.data || data.success)
            return true
          } else {
            commit('FAILED', data)
          }
        } catch (error) {
          if (error.response) {
            commit('FAILED', error.response.data)
          } else {
            console.error(error) // eslint-disable-line no-console
            commit('FAILED', error)
          }
        }
        return false
      },
      async clear({ commit }) {
        commit('CLEAR')
      },
    },
    state: {
      loading: false,
      data: null,
      error: null,
    },
    mutations: {
      CLEAR: (state) => Object.assign(state, { loading: false, data: null, error: null }),
      LOADING: (state) => Object.assign(state, { loading: true }),
      SUCCESS: (state, data) => Object.assign(state, { loading: false, data, error: null }),
      FAILED: (state, error) => Object.assign(state, { loading: false, data: null, error }),
    },
  }
}
